<?php
namespace QueueJobs;

abstract class Job
{
	const STATUS_QUEUED = 1;
	const STATUS_STARTED = 2;
	const STATUS_ERROR = 3;
	const STATUS_FINISHED = 4;
	public static $statusMap = [
		self::STATUS_QUEUED => 'Queued',
		self::STATUS_STARTED => 'Started',
		self::STATUS_ERROR => 'Error',
		self::STATUS_FINISHED => 'Finished',
	];
	public $id;
	public $ns;
	public $type;
	public $data = array();
	public function __construct($type, $ns = 'global')
	{
		$this->type = $type;
		$this->ns = $ns;
		
		$this->id = $this->generateId();
		$this->persister = Persistence\Redis::Instance();
	}
	abstract public function run();

	public function start($msg)
	{
		$this->started($this);
		try {
			$this->run();
		} catch (Exception $e) {
			$errorMsg = '';
			// if ($e instanceof WorkerException) {
				$errorMsg = $e->getMessage();
			// }
			$this->errored($errorMsg);
		}
		$this->finished($msg);
	}

	public function progress($value)
	{
		$this->persister->incr($this->id, 'progress', $value);
	}

	public function queued()
	{
		$this->persister->save($this->id, array(
			'type' => $this->type,
			'ns' => $this->ns,
			'status' => self::STATUS_QUEUED,
			'queued' => time(),
		));
	}
	public function started()
	{
		$this->persister->save($this->id, array(
			'status' => Job::STATUS_STARTED,
			'progress' => 0,
			'started' => time(),
		));
	}
	public function finished($msg)
	{
		$this->persister->save($this->id, array(
			'status' => Job::STATUS_FINISHED,
			'progress' => 100,
			'finished' => time(),
		));
		$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
	}
	public function errored($msg)
	{
		$this->persister->save($this->id, array(
			'status' => Job::STATUS_ERROR,
			'error' => $msg,
		));
	}

	private function generateId()
	{
		return uniqid();
	}
}