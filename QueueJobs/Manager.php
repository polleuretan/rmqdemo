<?php
namespace QueueJobs;

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Manager
{
	private $channel;
	private $connection;

	public function __construct()
	{
		$rmqConfig = Config::get('rmq');
		$this->connection = new AMQPConnection(
			$rmqConfig['host'],
			$rmqConfig['port'],
			$rmqConfig['user'],
			$rmqConfig['pass']
		);
		$this->channel = $this->connection->channel();
		$this->persister = Persistence\Redis::Instance();
	}
	public function __destruct()
	{
		$this->channel->close();
		$this->connection->close();
	}
	public function qLen($type)
	{
		$qInfo = array_pad($this->channel->queue_declare($type, true), 3, 0); 
		return $qInfo[1];
	}
	public function addJob(Job $job)
	{
		$msg = new AMQPMessage(serialize($job), array('delivery_mode' => 2));

		$this->declareTypeQ($job->type);
		$this->channel->basic_publish($msg, '', $job->type);
		$job->queued();
	}
	public function getJobs($by = array())
	{
		if (!$by) {
			return $this->persister->all();
		} else {
			$value = reset($by);
			$attr = key($by);
			return $this->persister->findBy($attr, $value);
		}
	}
	public function deleteJob($id)
	{
		$this->persister->delete($id);
	}
	public function consumeJobs($type, $prefetchCount = 1)
	{
		$this->declareTypeQ($type);

		$callback = function($msg){
			$job = unserialize($msg->body);
			$job->start($msg);
		};
		$this->channel->basic_qos(null, $prefetchCount, null);
		$this->channel->basic_consume($type, '', false, false, false, false, $callback);
		while(count($this->channel->callbacks)) {
			$this->channel->wait();
		}
	}
	private function declareTypeQ($type)
	{
		$this->channel->queue_declare($type, false, true, false, false);
	}
}