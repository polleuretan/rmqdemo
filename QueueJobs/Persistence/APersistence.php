<?php
namespace QueueJobs\Persistence;

abstract class APersistence
{
	public static function Instance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new static();
		}
		return $inst;
	}
	protected function __construct(){}

	abstract public function save($id, $data);
	abstract public function incr($id, $attr, $value);
	abstract public function all();
	abstract public function findBy($attrName, $attrValue);
}