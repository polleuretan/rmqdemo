<?php
namespace QueueJobs\Persistence;

use QueueJobs\Config;
class Redis extends APersistence
{
	protected $client;
	protected $prefix = 'qj_';
	protected $fields = array(
		'type',
		'ns',
		'status',
		'progress',
		'queued',
		'started',
		'finished',
	);
	protected $indexFields = array(
		'type',
		'ns',
		'status',
	);
	protected function __construct()
	{
		$redisConfig = Config::get('redis');
		$this->client = new \Predis\Client([
				'scheme' => $redisConfig['scheme'],
				'host' => $redisConfig['host'],
				'port' => $redisConfig['port'],
		]);
	}
	public function delete($id)
	{
		$data = $this->client->hgetall($this->composeKey($id));
		foreach ($this->indexFields as $name) {
			if (isset($data[$name])) {
				$this->cleanIndex($id, $name, $data[$name]);
			}
		}
		$this->cleanPrimaryIndex($id);
		$this->client->del($this->composeKey($id));
	}
	public function save($id, $data)
	{
		$hash = array();
		$hash['id'] = $id;
		foreach ($this->fields as $name) {
			if (isset($data[$name])) {
				$hash[$name] = $data[$name];
			}
		}
		$this->client->hmset($this->composeKey($id), $hash);
		foreach ($this->indexFields as $name) {
			if (isset($data[$name])) {
				$this->maintainIndex($id, $name, $data[$name]);
			}
		}
		$this->maintainPrimaryIndex($id);
	}
	public function incr($id, $attr, $value)
	{
		$this->client->hincrby($this->composeKey($id), $attr, $value);
	}
	public function all()
	{
		$index = $this->composeIndexName('primary');
		$keys = $this->client->smembers($index);

		return $this->getAllByKeys($keys);
	}
	public function findBy($attrName, $attrValue)
	{
		$index = $this->composeIndexName($attrName);
		$index .= ':' . $attrValue;
		$keys = $this->client->smembers($index);

		return $this->getAllByKeys($keys);
	}
	private function getAllByKeys($keys)
	{
		$pipe = $this->client->pipeline();
		foreach ($keys as $key) {
			$pipe->hgetall($key);
		}
		return $pipe->execute();
	}
	private function maintainIndex($id, $attr, $value)
	{
		$index = $this->composeIndexName($attr);
		$index .= ':' . $value;
		$this->client->sadd($index, $this->composeKey($id));
	}
	private function maintainPrimaryIndex($id)
	{
		$index = $this->composeIndexName('primary');
		$this->client->sadd($index, $this->composeKey($id));
	}
	private function cleanIndex($id, $attr, $value)
	{
		$index = $this->composeIndexName($attr);
		$index .= ':' . $value;
		$this->client->srem($index, $this->composeKey($id));
	}
	private function cleanPrimaryIndex($id)
	{
		$index = $this->composeIndexName('primary');
		$this->client->srem($index, $this->composeKey($id));
	}
	private function composeIndexName($attr)
	{
		return $this->prefix . 'index_' . $attr;
	}
	private function composeKey($id)
	{
		return $this->prefix . $id;
	}
}