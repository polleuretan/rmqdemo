<?php
namespace QueueJobs;

class Config
{
	public static $data = array(
		'rmq' => array(
			'host' => 'localhost',
			'port' => 5672,
			'user' => 'guest',
			'pass' => 'guest',
			'exchange' => 'queue_jobs',
		),
		'redis' => array(
			'scheme' => 'tcp',
			'host' => '127.0.0.1',
			'port' => 6379,
		)
	);

	private static $configFile;
	private static $configRead = false;

	public static function setConfigFile($path)
	{
		self::$configFile = $path;
	}

	public static function get($attrName)
	{
		self::readConfig();
		if (isset(self::$data[$attrName])) {
			return self::$data[$attrName];
		}
	}
	private static function readConfig()
	{
		if (!self::$configRead && file_exists(self::$configFile)) {
			$config = require(self::$configFile);
			self::$data = array_merge(self::$data, $config);
			self::$configRead = true;
		}
	}
}